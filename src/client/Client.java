package client;

import java.io.IOException;
import java.net.Socket;

public class Client {

    private String username;
    private String hostname;
    private int port;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Client(String hostname, int port){
        this.hostname = hostname;
        this.port = port;
    }

    public void start(){

        try {
            Socket socket = new Socket(hostname, port);
            System.out.println("Connected to server");
            new ReadThread(socket, this).start();
            new WriteThread(socket, this).start();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
