package client;


public class Main {

    private final static String HOSTNAME="localhost";
    private final static int PORT = 8080;


    public static void main(String[] args){

        Client client = new Client(HOSTNAME,PORT);
        client.start();
    }

}
