package client;

import java.io.Console;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class WriteThread extends Thread{

    private PrintWriter writer;
    private Socket socket;
    private Client client;

    public WriteThread(Socket socket, Client client) {
        this.socket = socket;
        this.client = client;

        try {
            OutputStream output = socket.getOutputStream();
            writer = new PrintWriter(output, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void run() {

        System.out.println("enter username");
        String userName = new Scanner(System.in).nextLine();
        client.setUsername(userName);
        writer.println(userName);

        String text;

        do {
           // text = System.console().readLine("<" + userName + ">: ");
            text = new Scanner(System.in).nextLine();
            writer.println(text);

        } while (!text.equals("q"));

        try {
            socket.close();
        } catch (IOException e) {

            e.printStackTrace();

        }
    }
}
