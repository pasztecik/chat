package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;

public class ChatServer {

    private int port;
    private Set<User> users = new HashSet<>() ;

    public int getPort() {
        return port;
    }

    public ChatServer(int port){
        this.port = port;
    }

    public void startServer(){
        try {
            ServerSocket serverSocket = new ServerSocket(port);
            System.out.println("Server started on port: "+port);

            while(true){
                Socket socket = serverSocket.accept();
                System.out.println("Someone has connected");

                User newUser = new User(socket, this);
                users.add(newUser);
                newUser.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void removeUser(User user){

        if(users.remove(user)){
            System.out.println("User: "+user.getUsername()+" has disconnected");
        }
    }

    public Set<User> getUsers(){
        return this.users;
    }

    public void sendMessage(String message, User user){
        for (User theUser : users) {
                theUser.sendMessage(message);
        }

    }

}
