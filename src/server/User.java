package server;

import server.chatHelper.ConsoleColor;

import java.io.*;
import java.net.Socket;

public class User extends Thread {

    private String username;
    private Socket socket;
    private ChatServer server;
    private PrintWriter writer;
    private ConsoleColor color;

    public User(Socket socket, ChatServer server){
        this.socket=socket;
        this.server=server;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void run(){
        try{
            InputStream input = socket.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));
            OutputStream output = socket.getOutputStream();
            writer = new PrintWriter(output, true);

            String username = reader.readLine();
            setUsername(username);

            String welcomeMessage =ConsoleColor.CYAN+ "Welcome: "+username+"on the server";
            server.sendMessage(welcomeMessage,this);

            handleMessageSending(reader);
            server.removeUser(this);
            socket.close();

            String quittingMessage = this.username + " has quit";
            server.sendMessage(quittingMessage,this);
        }catch(IOException e){
            e.printStackTrace();
        }
    }
    public void handleMessageSending(BufferedReader reader) throws IOException {
        String clientMessage;
        String serverMessage;
        do{
            clientMessage =  reader.readLine();
            serverMessage = "<"+this.username+">: "+clientMessage;
            server.sendMessage(serverMessage, this);
        }while(!clientMessage.equals("q"));

    }

    void sendMessage(String message){
        writer.println(message);
    }



}
