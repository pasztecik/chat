package server;

public class Main {

    private final static int PORT = 8080;

    public static void main(String[] args){

        ChatServer server = new ChatServer(PORT);
        server.startServer();
    }


}
